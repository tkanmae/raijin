Flask==0.10.1
geojson==1.0.7
itsdangerous==0.24
Jinja2==2.7.3
MarkupSafe==0.23
requests==2.3.0
uWSGI==2.0.6
Werkzeug==0.9.6
