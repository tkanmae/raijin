# -*- coding: utf-8 -*-
from __future__ import division

import datetime
import logging
import os
import time
import re
from xml.etree import ElementTree
import requests
from . import gdal, BASE_DIR


_re_latest_info = re.compile(
    r'GOES (\d{2}) .* at UTC (\d{2}:\d{2}:\d{2}).*day (\d+) of')


logger = logging.getLogger('app.task.goes_ir4')


def _timedelta_to_minutes(duration):
    """Convert an ``timedelta`` object to minutes."""
    days, seconds = duration.days, duration.seconds
    return days * 1440 + seconds / 60


def _add_palette(src, dst):
    """Add LUT to a VRT file."""
    tree = ElementTree.parse(src)

    vrt_raster_band = tree.find('.//VRTRasterBand')

    color_interp = vrt_raster_band.find('.//ColorInterp')
    color_interp.text = 'Palette'

    color_table = ElementTree.Element('ColorTable')

    with open(os.path.join(BASE_DIR, 'data/palette.txt')) as fh:
        for l in fh:
            if l.startswith('#'):
                continue
            c = l.strip().split()
            attrs = {'c1': c[0], 'c2': c[1], 'c3': c[2], 'c4': '255'}
            ElementTree.SubElement(color_table, 'Entry', **attrs)

    vrt_raster_band.append(color_table)
    tree.write(dst)


def fetch_latest_image(sector, ew, newer=None):
    """Fetch the latest image from goes.gsfc.nasa.gov.

    Parameters
    ----------
    sector: str
        Sector.
    ew: {'east', 'west'}
        Designation of the GOES satellite.
    newer: datetime, optional
        Fetch the image if the latest image on the server is newer than
        ``newer``.

    Returns
    -------
    res: 3-tuple
        Return the GOES number, recording time, and image.  The recording time
        is a ``datetime`` object, and the image is a byte string.  Return
        ``None`` if there is no image to be retrieved.
    """
    strftime = datetime.datetime.strftime
    strptime = datetime.datetime.strptime

    if sector not in ('conus',):
        raise ValueError('sector not found: %s' % sector)

    base_urls = {
        'east': 'http://goes.gsfc.nasa.gov/goeseast-lzw/%s/ir4/' % sector,
        'west': 'http://goes.gsfc.nasa.gov/goeswest-lzw/%s/ir4/' % sector,
    }

    url = base_urls[ew] + 'latest.info'
    logger.debug('Downloading %s' % url)
    response = requests.get(url)
    if not response.ok:
        logger.error('Failed to get %s' % url)

    goes_num, hms, days = _re_latest_info.search(response.content).groups()
    year = strftime(datetime.datetime.utcnow(), '%Y')
    rtime = strptime('{0} {1} {2}'.format(year, days, hms), '%Y %j %H:%M:%S')

    if newer is None or rtime > newer:
        url = base_urls[ew] + 'latest.tif'
        logger.info('Downloading %s' % url)
        response = requests.get(url)
        if not response.ok:
            logger.error('Failed to get %s' % url)
        buf = response.content
        return (goes_num, rtime, buf)
    else:
        return None


def create_wms_ready_image(src, dst, sector, extent_epsg4326, size):
    """Warp an image to EPSG:3857 projection."""
    if 'G13' in src or 'G14' in src:
        ew = 'east'
    elif 'G15' in src:
        ew = 'west'
    else:
        logger.error('Could not identify the GOES designation.')

    tmps = (
        '/tmp/app.task.goes_ir4/tmp1.vrt',
        '/tmp/app.task.goes_ir4/tmp2.vrt',
        '/tmp/app.task.goes_ir4/tmp3.vrt',
        '/tmp/app.task.goes_ir4/tmp4.tif',
    )
    if not os.path.isdir(os.path.dirname(tmps[0])):
        os.makedirs(os.path.dirname(tmps[0]))

    logger.debug('Creating a WMS image.')
    try:
        # Step 1: Add GCPs to the raw TIFF image.  Note that the results of
        # intermediate processes from Step 1 through 4 are written to VRT files.
        # The VRT files as well as the raw TIFF image are used to convert to
        # produce the resultant TIFF image.
        gdal.append_gcps(src, tmps[0], sector, ew)

        # Step 2: warp the raw data to EPSG:4326.
        opts_common = '-q -overwrite -order 3 -r bilinear -et 0.075 '
        opt_ts = '-ts {0[0]} {0[1]}'.format(size)
        opt_te = '-te {0[0]} {0[1]} {0[2]}, {0[3]}'.format(extent_epsg4326)
        opt_srs = '-t_srs epsg:4326'
        gdal.warp(tmps[0], tmps[1], [opts_common, opt_srs, opt_ts, opt_te])

        # Step 3: warp the data from EPSG:4326 to EPSG:3857.
        opt_srs = '-s_srs epsg:4326 -t_srs epsg:3857'
        gdal.warp(tmps[1], tmps[2], [opts_common, opt_srs, opt_ts])

        # Step 4: warp the data from EPSG:4326 to EPSG:3857.
        _add_palette(tmps[2], tmps[2])

        # Step 5: finally, create the resultant TIFF file from the VRT files and
        # the ras TIFF image.
        gdal.warp(tmps[2], tmps[3], ['-q -overwrite -co "TILED=yes"', opt_ts])
        os.rename(tmps[3], dst)

        logger.info('{0} created.'.format(dst))
    finally:
        for tmp in tmps:
            if os.path.isfile(tmp):
                os.remove(tmp)


class Task(object):

    def __init__(self, dpath, wms_extent, wms_size):
        """
        Parameters
        ----------
        dpath: str
            Directory path where the resultant images are stored.
        wms_extent: 4-tuple of floats
            Extent of the resultant image in EPSG:4326.
        wms_size: 2-tuple of ints
            Size of the resultant image.
        """
        self._dpath = os.path.abspath(dpath)
        self._sector = 'conus'
        self._wms_extent = wms_extent
        self._wms_size = wms_size

    def run(self, interval):
        """
        Parameters
        ----------
        interval: int
            Interval in seconds.
        """
        logger.info('Starting up.')

        delta = datetime.timedelta(minutes=9)

        time_recorded, fpath = self._fetch('east', None)
        newer = time_recorded + delta
        if fpath:
            self._create_wms_ready_image(fpath, fpath)
        self._wait(interval)

        num_nodata_cycles = 0
        max_nodata_cycles = 28 * 60 // interval

        while True:
            res = self._fetch('east', newer)
            if res:
                time_recorded, fpath = res
                self._create_wms_ready_image(fpath, fpath)
                newer = time_recorded + delta
                num_nodata_cycles = 0
                logger.debug('num_nodata_cycles = %d' % num_nodata_cycles)
            else:
                num_nodata_cycles += 1
                logger.debug('num_nodata_cycles = %d' % num_nodata_cycles)

            if num_nodata_cycles > max_nodata_cycles:
                self._wait(10)
                res = self._fetch('west', newer)
                if res:
                    time_recorded, fpath = res
                    self._create_wms_ready_image(fpath, fpath)
                    newer = time_recorded + delta
                    num_nodata_cycles = 0

            self._wait(interval)

    def _fetch(self, ew, newer):
        res = fetch_latest_image(self._sector, ew, newer)
        if res:
            goes_num, time_recorded, buf = res
            fname = self._filename(goes_num, time_recorded)
            fpath = os.path.join(self._dpath, fname)
            open(fpath, 'wb').write(buf)
            return time_recorded, fpath
        else:
            return None

    def _remove_older_images(self):
        num = 300

        fnames = []
        for fname in os.listdir(self._dpath):
            if fname.endswith('.tif') and not fname.startswith('latest'):
                fnames.append(fname)

        if len(fnames) > num:
            fnames.sort()
            logger.info('Deleting old images.')
            for i in range(len(fnames) - num):
                os.remove(os.path.join(self._dpath, fnames.pop(0)))

    def _create_wms_ready_image(self, src_fpath, dst_fpath):
        create_wms_ready_image(src_fpath, dst_fpath, self._sector,
                               self._wms_extent, self._wms_size)
        self._update_symlink_to_latest_image(dst_fpath)
        self._remove_older_images()

    def _update_symlink_to_latest_image(self, fpath):
        """"Update a symlink 'latest.tif' pointing to a newly created image."""
        lpath = os.path.join(self._dpath, 'latest.tif')
        if os.path.islink(lpath):
            os.remove(lpath)
        os.symlink(fpath, lpath)

    def _filename(self, goes_num, time_recorded):
        strftime = datetime.datetime.strftime
        fmt = '%y%m%d%H%MG{0}I04.tif'
        fname = strftime(time_recorded, fmt).format(goes_num)
        return fname

    def _wait(self, interval):
        logger.debug('Waiting for {0} sec.'.format(interval))
        time.sleep(interval)

    def __del__(self):
        logger.info('Shutting down.')
