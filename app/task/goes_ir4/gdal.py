# -*- coding: utf-8 -*-
"""This module provides functionalities of GDAL utilities and some helper
functions."""
from __future__ import division

import os
import shlex
import subprocess
from . import BASE_DIR


# Check the version number of gdal.
try:
    ret = subprocess.check_output(['gdal-config', '--version'])
    major, minor, micro = map(int, ret.strip().split('.'))
    if major < 0:
        raise RuntimeError
    elif major == 1 and minor < 9:
            raise RuntimeError
except:
    raise RuntimeError('gdal>=1.9.0 must be available.')


__all__ = ['translate', 'warp', 'append_gcps']


def _flatten(l):
    """Flatten a nested list."""
    if isinstance(l, (list, tuple)):
        for sub in l:
            for x in _flatten(sub):
                yield x
    else:
        yield l


def _parse_command_list(cmds):
    return shlex.split(' '.join(_flatten(cmds)))


def _load_gcps(sector, ew):
    """Return GCPs.

    Parameters
    ----------
    sector: {'conus', 'colorado'}
        Sector.
    ew: {'east', 'west'}
        Designation of the GOES satellite.

    Returns
    -------
    gcps: list of 4-tuples of floats
        GSPs.
    """
    ew = ew.lower()
    if ew not in ('east', 'west'):
        raise ValueError("ew must be either 'east' or 'west'")

    tail = 'G13I04.gcp' if ew == 'east' else 'G11I04.gcp'
    fname = sector + '_' + tail

    with open(os.path.join(BASE_DIR, 'data', fname)) as fh:
        gcps = []
        for l in fh:
            if l.startswith('#'):
                continue
            gcps.append(l.strip())
    return gcps


def translate(src, dst, opts):
    """Wrapper for `gdal_translate`.

    Parameters
    ----------
    src: str
        Filename of a source data set.
    dst: str
        Filename of a target data set.
    opts: str, optional
        Option string.
    """
    if dst.endswith('.vrt'):
        opts.append(' -of VRT')
    elif dst.endswith('.tif'):
        opts.append(' -of GTiff')
    else:
        raise ValueError('Output format not supported: {0}'.format(dst))

    args = ['gdal_translate']
    args.extend([opts, src, dst])
    args = _parse_command_list(args)
    subprocess.check_output(args, stderr=subprocess.STDOUT)


def warp(src, dst, opts):
    """wrapper for `gdalwarp`.

    Parameters
    ----------
    src: str
        File name of a source data set.
    dst: str
        File name of a target data set.
    opts: str, optional
        Option string.

    """
    if dst.endswith('.vrt'):
        opts.append(' -of VRT')
    elif dst.endswith('.tif'):
        opts.append(' -of GTiff')
    else:
        raise ValueError('Output format not supported: {0}'.format(dst))

    args = ['gdalwarp']
    args.extend([opts, src, dst])
    args = _parse_command_list(args)
    subprocess.check_output(args, stderr=subprocess.STDOUT)


def append_gcps(src, dst, sector, ew):
    """Create a GeoTIFF image with given GCPs embedded.

    Parameters
    ----------
    src: str
        File name of a source data set.
    dst: str
        File name of a target data set.
    sector: {'conus', 'colorado'}
        Sector.
    ew: {'east', 'west'}
        Designation of the GOES satellite.

    Returns
    -------
    output: str
        Output.
    """
    opts = ['-q']
    for gcp in _load_gcps(sector, ew):
        opts.append('-gcp')
        opts.append(gcp)

    return translate(src, dst, opts)
