Raijin
======


## On Local Machine

Create a Python virtual environment in the current directory:

```shell-session
$ virtualenv env
...
$ source env/bin/activate
```

and install Python packages:

```shell-session
(env)$ pip install -r reuirements.txt -r dev-requirements.txt
...
```

You can launch a HTTP server that comes with Flask:

```shell-session
(env)$ ./run.py
 * Running on http://127.0.0.1:5000/
 * Restarting with reloader
...
```


## On a Vagrant Managed VM

### Bootstrapping

To avoid some glitches in establishing SSH connection to Vagrant managed VMs
via Ansible, you may want to add the lines look like below to `~/.ssh/config`:

```
Host 192.168.33.*
  User vagrant
  IdentityFile ~/.vagrant.d/insecure_private_key
  StrictHostKeyChecking no
```


### Provisioning

Start up a VM for development via Vagrant:

```shell-session
$ vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Checking if box 'ubuntu/trusty64' is up to date...
...
```

This mounts the local current directory at `/var/www/code` in the VM as a
synced folder.  The command `vagrant rsync-auto` will watch changes in the
local directory and will automatically sync the contents in `/var/www/code`
with those in the local directory.


Provision the VM:

```shell-session
$ ansible-playbook -l development playbook/provision.yml -u vagrant
```


### Deployment

Deploy the application on the VM:

```shell-session
$ ansible-playbook -l production playbook/deploy.yml -u vagrant
```


## On a Production Server

### Bootstrapping

First of all, you need to add FQDN or IP address of a production server to the
inventory `playbooks/hosts`.  After editing the inventory, you may want to
check if you can access the production server via Ansible:

```shell-session
$ ansible production -m ping -u root
host | success >> {
    "changed": false,
    "ping": "pong"
}
```

If the command above throws an error, ensure that `~/.ssh/known_hosts` does not
have an entry with the FQDN or IP address of the production server.


### Provisioning

Provision the production server:

```shell-session
$ ansible-playbook -l production playbook/provision.yml -u root
```

In addition to installing and configuring software, this will create a user
`www-data` whose home directory is `/var/www` and generate SSH keys in
`/var/www/.ssh`.

In order to deploy the code in the next step, you need to register the SSH
public key to a repository hosted at BitBucket.  Go to [the BitBucket
page](https://bitbucket.org/tkanmae/raijin), and add the public key
(`/var/www/.ssh/id_rsa.pub`) as a development key.  This will provide
`www-data` read-only access to the repository.


### Deployment

Deploy the application on the production server:

```shell-session
$ ansible-playbook -l production playbook/deploy.yml -u root
```
