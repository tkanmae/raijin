#!env/bin/python
# -*- coding: utf-8 -*-
from __future__ import division

import argparse
import logging
import os
import sys
from logging.handlers import TimedRotatingFileHandler

from app.task import cmc, goes_ir4


def run_goes_ir4(args):
    WMS_EXTENT = (-125.0, 20.0, -65.0, 50.0)
    WMS_SIZE = (1280, 768)
    INTERVAL = 90

    task = goes_ir4.Task(args.output_dir, WMS_EXTENT, WMS_SIZE)
    task.run(INTERVAL)


def run_cmc(args):
    task = cmc.Task(args.output_dir)
    task.run()


def init_logger(filepath):
    logger = logging.getLogger('app.task')
    logger.setLevel(logging.DEBUG)

    fmt = logging.Formatter(
        '%(asctime)s %(name)s %(levelname)s %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S")

    if filepath:
        handler = TimedRotatingFileHandler(filepath, when='D', backupCount=3)
    else:
        handler = logging.StreamHandler(sys.stdout)

    handler.setFormatter(fmt)
    handler.setLevel(logging.DEBUG)

    logger.addHandler(handler)
    return logger


def init_argparser(parser):
    subparsers = parser.add_subparsers()

    parser_goes_ir4 = subparsers.add_parser('goes_ir4')
    parser_goes_ir4.add_argument(
        '-l', '--logfile', type=str, default='',
        help='specify path to a log file; otherwise use stdout')
    parser_goes_ir4.add_argument(
        'output_dir', metavar='OUTPUT_DIR',
        help='specify output directory')
    parser_goes_ir4.set_defaults(func=run_goes_ir4)

    parser_cmc = subparsers.add_parser('cmc')
    parser_cmc.add_argument(
        '-l', '--logfile', type=str, default='',
        help='specify path to a log file; otherwise use stdout')
    parser_cmc.add_argument(
        'output_dir', metavar='OUTPUT_DIR',
        help='specify output directory')
    parser_cmc.set_defaults(func=run_cmc)

    return parser


def main(args):
    fpath = args.logfile
    if not os.path.isdir(os.path.dirname(fpath)):
        fpath = ''
    else:
        fpath = os.path.abspath(fpath)

    init_logger(fpath)

    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)

    args.func(args)


if __name__ == '__main__':
    parser = init_argparser(argparse.ArgumentParser())
    args = parser.parse_args()

    main(args)
