# -*- coding: utf-8 -*-
from __future__ import absolute_import, division

import collections
import logging
import os
import time
import re
from datetime import datetime, timedelta
import geojson
import requests


URL = 'http://people.ee.duke.edu/~cummer/cmc/markers.js'

logger = logging.getLogger('app.task.cmc')


def download(uri):
    try:
        response = requests.get(uri)
    except Exception as exc:
        raise exc
    if not response.ok:
        raise RuntimeError('failed to download: %s' % uri)
    return response.content


def extract_strikes(content):
    # Extract strike data in an array `bigNewList`.
    m = re.search(r'var bigListNew = \[(.*)\];', content)
    if m is None:
        raise RuntimeError('failed to parse bigListNew')
    bignewlist = re.findall(r'\((.*?)\)', m.groups()[0])
    # Each element is a tuple of latitude, longitutde and magnitude.  They are
    # strings not numbers.
    data = [tuple(_.split(',')) for _ in bignewlist]
    return data


def extract_last_updated(content):
    m = re.search(r'return "(.+)";', content)
    if m is None:
        raise RuntimeError('failed to parse last_updated')
    last_updated = datetime.strptime(m.groups()[0], '%d-%b-%Y %H:%M:%S')
    return last_updated


def save_as_geojson(fpath, strikes):
    features = []
    for i, _ in enumerate(strikes, start=1):
        age = 5 * i

        # Bins the data according the magnitude of strokes.
        bins = {1: [], 2: [], 3: [], -1: [], -2: [], -3: []}
        for lat, lon, mag in _:
            sign = 1 if mag > 0 else -1
            if abs(mag) > 300:
                bins[sign*3].append((lon, lat))
            elif 100 < abs(mag) <= 300:
                bins[sign*2].append((lon, lat))
            else:
                bins[sign*1].append((lon, lat))

        # Creates MultiPoint features and bins them according to the time and
        # magnitude of strikes.
        for magnitude, items in bins.items():
            points = geojson.MultiPoint(coordinates=items)
            properties = {'age': age, 'magnitudeIndex': magnitude}
            f = geojson.Feature(geometry=points, properties=properties)
            features.append(f)

    with open(fpath, 'w') as fh:
        geojson.dump(geojson.FeatureCollection(features), fh)


class Task(object):
    def __init__(self, dpath):
        """
        Parameters
        ----------
        dpath: str
            Directory path where the resultant images are stored.
        """
        self._dpath = os.path.abspath(dpath)
        self._strikes_queue = collections.deque(maxlen=9)

        self._prev_strikes = None
        self._prev_update = None

    def run(self):
        ok = self._download_data()

        while True:
            next_update = self._prev_update + timedelta(minutes=5, seconds=30)
            self._wait((next_update - datetime.utcnow()).seconds)

            ok = self._download_data()
            if not ok:
                while True:
                    self._wait(30)
                    ok = self._download_data()
                    if ok:
                        break

    def _wait(self, seconds):
        logger.debug('wait for %d sec' % seconds)
        time.sleep(seconds)

    def _download(self):
        logger.debug('start downloading: %s' % URL)
        try:
            content = download(URL)
        except Exception as exc:
            logger.error('failed to download: %s' % exc)
            return None
        logger.debug('finish downloading')

        try:
            strikes = extract_strikes(content)
            last_update = extract_last_updated(content)
        except RuntimeError as exc:
            logger.error(exc)
            return None

        return strikes, last_update

    def _download_data(self):
        _ = self._download()
        if _:
            strikes, last_update = _
        else:
            return False

        if last_update != self._prev_update or self._prev_update is None:
            if self._prev_update is not None:
                new_strikes = list(set(strikes) - set(self._prev_strikes))
            else:
                new_strikes = strikes
            self._strikes_queue.appendleft(new_strikes)

            self._prev_strikes = strikes
            self._prev_update = last_update

            self._serialize()
            return True
        else:
            return False

    def _serialize(self):
        # Conver strike data to numbers.
        strikes = []
        for x in self._strikes_queue:
            strikes.append([tuple(map(float, _)) for _ in x])

        # Save the latest data in a text file.
        fpath = self._prev_update.strftime('%Y%m%d%H%M%S') + '.txt'
        fpath = os.path.join(self._dpath, fpath)
        with open(fpath, 'w') as fh:
            for x in strikes[0]:
                fh.write('%7.4f %9.4f %6.1f\n' % x)

        # Save the latest 15 min data in GeoJSON format.
        fpath = os.path.join(self._dpath, 'latest-15min.json')
        save_as_geojson(fpath, strikes)
