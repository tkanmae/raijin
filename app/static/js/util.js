var util = (function() {

  var PI = Math.PI;
  var DEG2RAD = PI / 180;
  var RAD2DEG = 180 / PI;

  var EPSG4326 = new OpenLayers.Projection('EPSG:4326');
  var EPSG3857 = new OpenLayers.Projection('EPSG:3857');

  var forwardMercator = OpenLayers.Layer.SphericalMercator.forwardMercator;
  var inverseMercator = OpenLayers.Layer.SphericalMercator.inverseMercator;

  var parseIsoUtcDate = function parseIsoUtcDate(str) {
    var x = str.match(/(\d+)/g);
    return Date.UTC(x[0], x[1] - 1, x[2], x[3], x[4], [5]);
  };

  var utcTime = function utcTime(date) {
    var tmp = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
      date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    return tmp;
  };

  var relativeAzimuth = function relativeAzimuth(az, hd) {
    if (hd === null) {
      return az;
    }
    az = az < 0 ? 360 + az : az;
    var relAz = az - (hd - 90);
    return relAz > 180 ? relAz - 360 : relAz;
  };

  var Observer = function(args) {
    this.name = args.name || '';
    this._setPosition(args);
    var xy = forwardMercator(this.lon, this.lat);
    this.geometryPoint = new OpenLayers.Geometry.Point(xy.lon, xy.lat);
    this.groundTrack = [];
    this._onMove = null;
  };
  Observer.prototype = {
    getLos: function(p2) {
      p2.alt = p2.alt || 70000.0;
      return inverseLos(this, p2);
    },
    moveTo: function(pos) {
      this._setPosition(pos);
      this._updateGeometryPoint();
      if (this._onMove !== null) {
        this._onMove();
      }
    },
    registerOnMove: function(callback) {
      this._onMove = callback;
    },
    _updateGeometryPoint: function() {
      var tmp = this.geometryPoint;
      var xy = forwardMercator(this.lon, this.lat);

      this.geometryPoint.move(xy.lon - tmp.x, xy.lat - tmp.y);
      this.groundTrack.push(new OpenLayers.Geometry.Point(xy.lon, xy.lat));
      if (this.groundTrack.length > 360) {
        this.groundTrack.shift();
      }
    },
    _setPosition: function(pos) {
      this.lon = parseFloat(pos.lon);
      this.lat = parseFloat(pos.lat);
      this.alt = parseFloat(pos.alt);
      this.head = parseFloat(pos.head) || 0;
      this.time = pos.time || null;
    }
  };


  var inverseVincentry = function inverseVincentry(p1, p2) {
    // WGS-84 ellipsoid parameters
    var a = 6378137, b = 6356752.314245, f = 1/298.257223563;

    var L = DEG2RAD * (p2.lon - p1.lon);
    var U1 = Math.atan((1-f) * Math.tan(DEG2RAD * p1.lat));
    var U2 = Math.atan((1-f) * Math.tan(DEG2RAD * p2.lat));
    var sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
    var sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);

    var lambda = L, sinLambda = 0, cosLambda = 0, lambdaP = 2 * PI;
    var sigma = 0, sinSigma = 0, cosSigma = 0, cos2SigmaM = 0;
    var sinAlpha = 0, cosSqAlpha = 0;
    var C = 0;
    var iterLimit = 30;

    while (Math.abs(lambda-lambdaP) > 1e-12 && --iterLimit > 0) {
      sinLambda = Math.sin(lambda);
      cosLambda = Math.cos(lambda);
      sinSigma = Math.sqrt((cosU2*sinLambda) * (cosU2*sinLambda) +
        (cosU1*sinU2-sinU1*cosU2*cosLambda) * (cosU1*sinU2-sinU1*cosU2*cosLambda));
      if (sinSigma === 0) {
        return { distance: 0, azimuth: 0 };  // co-incident points
      }
      cosSigma = sinU1*sinU2 + cosU1*cosU2*cosLambda;
      sigma = Math.atan2(sinSigma, cosSigma);
      sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
      cosSqAlpha = 1 - sinAlpha*sinAlpha;
      cos2SigmaM = cosSigma - 2*sinU1*sinU2/cosSqAlpha;
      if (isNaN(cos2SigmaM)) {
        cos2SigmaM = 0;  // equatorial line: cosSqAlpha=0 (§6)
      }
      C = f/16*cosSqAlpha*(4+f*(4-3*cosSqAlpha));
      lambdaP = lambda;
      lambda = L + (1-C) * f * sinAlpha *
        (sigma + C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)));
    }
    if (iterLimit === 0) {
      return { distance: NaN, azimuth: NaN };  // formula failed to converge
    }

    var uSq = cosSqAlpha * (a*a - b*b) / (b*b);
    var A = 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)));
    var B = uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)));
    var deltaSigma = B*sinSigma*(cos2SigmaM+B/4*(cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)-
      B/6*cos2SigmaM*(-3+4*sinSigma*sinSigma)*(-3+4*cos2SigmaM*cos2SigmaM)));
    var s = b*A*(sigma-deltaSigma);

    var fwdAz = Math.atan2(cosU2*sinLambda,  cosU1*sinU2-sinU1*cosU2*cosLambda);
    return {distance: s, azimuth: RAD2DEG * fwdAz};
  };


  var inverseLos = function inverseLos(p1, p2) {
    // WGS-84 ellipsoid parameters
    var a = 6378137, b = 6356752.314245, f = 1/298.257223563;
    var ecsSqr1 = (a*a - b*b) / (a*a);
    var ecsSqr2 = (a*a - b*b) / (b*b);

    var tmp = inverseVincentry(p1, p2);
    var az = tmp.azimuth;
    var s = tmp.distance;

    // Compute the radius of curvature at given latitude and azimuth.
    var sinLat1 = Math.sin(DEG2RAD * p1.lat);
    var cosLat2 = Math.cos(DEG2RAD * p2.lat);
    var sinAz = Math.sin(DEG2RAD * az);
    var cosAz = Math.cos(DEG2RAD * az);
    var rho = a * (1 - ecsSqr1) / Math.pow(1 - ecsSqr1*sinLat1*sinLat1, 3/2);
    var nu = a / Math.sqrt(1 - ecsSqr1*sinLat1*sinLat1);
    var r = 1  / (cosAz*cosAz/rho + sinAz*sinAz/nu);

    var theta = s / r;
    var l01 = r + p1.alt;
    var l02 = r + p2.alt;
    var l12 = Math.sqrt(l01*l01 + l02*l02 - 2*l01*l02*Math.cos(theta));
    var l12p = l01 * Math.tan(theta);
    var l22p = l02 - l01 / Math.cos(theta);
    var delta = Math.acos(-(l22p*l22p - l12*l12 - l12p*l12p) / (2*l12*l12p));
    delta = l22p < 0 ? -1 * delta : delta;

    return {
      azimuth: az,
      elevation: RAD2DEG * delta,
      distance: s,
      length: l12
    };
  };

  var ObserverControl = OpenLayers.Class(OpenLayers.Control, {
    observer: null,

    _line: null,

    _mousePaused: true,

    initialize: function(options) {
      OpenLayers.Control.prototype.initialize.apply(this, options);

      if (!(options && options.layerOptions && options.layerOptions.styleMap)) {
        this.style = OpenLayers.Util.extend(
          OpenLayers.Feature.Vector.style['default'], {});
      }

      this.handler = new OpenLayers.Handler.Hover(this, {
          move: this.onMouseMove,
          pause: this.onMousePause,
          out: this.onMouseOut
        }, {
          delay: 600,
          pixelTolerance: null,
          stopMove: false,
          // Provides a callback 'out' when the mouse gets out of `map`.
          mouseout: function(evt) {
            if (OpenLayers.Util.mouseLeft(evt, this.map.viewPortDiv)) {
              this.clearTimer();
              this.callback('out', [evt]);
            }
            return true;
          }
        }
      );
    },

    activate: function() {
      if (OpenLayers.Control.prototype.activate.apply(this)) {
        // Set up layer.
        this.layer = new OpenLayers.Layer.Vector("Observer");
        this.map.addLayer(this.layer);
        // Create the navigation div element.
        $('<div/>', {
        }).addClass('utilObserverControlNav').appendTo('#map').hide();

        return true;
      } else {
        return false;
      }
    },

    deactivate: function() {
      if (OpenLayers.Control.prototype.deactivate.apply(this)) {
        if (this.layer.map !== null) {
          this.destroyFeature();
          this.layer.destory(false);
        }
        this.layer = null;
        return true;
      } else {
        return false;
      }
    },

    destroyFeature: function() {
      if (this.layer) {
        this.layer.destroyFeatures();
      }
    },

    setObserver: function(observer) {
      if (!this.active) {
        this.activate();
      }
      this.observer = observer;

      if (this._line !== null) {
        this.layer.destroyFeatures(this._line);
      }
      this._line = new OpenLayers.Feature.Vector(
        new OpenLayers.Geometry.LineString([this.observer.geometryPoint])
      );

      this.map.setCenter(new OpenLayers.LonLat(observer.lon, observer.lat)
        .transform(EPSG4326, EPSG3857));
    },

    onMouseMove: function(evt) {
      if (this._mousePaused) {
        this.layer.setVisibility(false);
        $('div.utilObserverControlNav').hide();
        this._mousePaused = false;
      }
    },

    onMousePause: function(evt) {
      this.layer.setVisibility(true);

      var xy = this.map.getLonLatFromPixel(evt.xy);
      this._line.geometry.components[1] =
        new OpenLayers.Geometry.Point(xy.lon, xy.lat);
      this._updateControlNav();

      this._mousePaused = true;
    },

    onMouseOut: function(evt) {
      this.layer.setVisibility(false);
    },

    _updateControlNav: function() {
      this.layer.drawFeature(this._line);

      var xy = this._line.geometry.components[1];
      var los = this.observer.getLos(inverseMercator(xy.x, xy.y));

      var distance = los.distance / 1000;
      var relAz = relativeAzimuth(los.azimuth, this.observer.head);

      var content = '';
      content = this.observer.name + '<br>' +
        'AZ (deg): ' + los.azimuth.toFixed(1) + '<br>' +
        'EL (deg): ' + los.elevation.toFixed(1) + '<br>' +
        'DS (mi/km): ' + (distance / 1.61).toFixed(1) + '/' + distance.toFixed(1);

      var px = this.map.getPixelFromLonLat(new OpenLayers.LonLat(xy.x, xy.y));

      var mapDiv = $('#map');
      var mapWidth = mapDiv.width();
      var mapHeight = mapDiv.height();

      var controlDiv = $('div.utilObserverControlNav').html(content);
      var controlWidth = controlDiv.width();
      var controlHeight = controlDiv.height();

      var x = 0;
      var y = 0;
      if (px.x + controlWidth + 15 > mapWidth) {
        x = px.x - controlWidth - 10;
      } else {
        x = px.x + 15;
      }
      if (px.y + controlHeight + 15 > mapHeight) {
        y = px.y - controlHeight - 10;
      } else {
        y = px.y + 15;
      }

      controlDiv.css({top: y, left: x}).show();
    },
  });

  return {
    parseIsoUtcDate: parseIsoUtcDate,
    utcTime: utcTime,
    Observer: Observer,
    ObserverControl: ObserverControl,
    inverseVincentry: inverseVincentry,
    inverseLos: inverseVincentry
  };
})();
