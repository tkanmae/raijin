var map = null;

var sites = {
  'langmuir': new util.Observer({
      name: 'Langmuir Laboratory',
      lon: -107.18072,
      lat: 33.97530,
      alt: 3255.0
    }),
  'douglas': new util.Observer({
      name: 'Douglas',
      lon: -105.3844,
      lat: 42.7561,
      alt: 1475.0
  }),
  'yuccaRidge': new util.Observer({
      name: 'Yucca Ridge',
      lon: -104.93983,
      lat: 40.66839,
      alt: 1654.0
  }),
  'lasVegas': new util.Observer({
      name: 'Las Vegas',
      lon: -105.080,
      lat: 35.555,
      alt: 2062.0
  }),
  'portales': new util.Observer({
      name: 'Portales',
      lon: -103.196,
      lat: 34.121,
      alt: 1202.0
  }),
  'sac': new util.Observer({
      name: 'SAC Peak',
      lon: -105.82,
      lat: 32.79,
      alt: 2800.0
  }),
  'wiro': new util.Observer({
      name: 'WIRO',
      lon: -105.97653,
      lat: 41.09711,
      alt: 2943.0
    }),
  'okLMA': new util.Observer({
      name: 'Oklahoma LMA',
      lon: -97.91786,
      lat: 35.27912,
      alt: 390.0
  }),
  'rapidCity': new util.Observer({
      name: 'Rapid City',
      lon: -103.25,
      lat: 44.05,
      alt: 1100.0
  }),
  'duke': new util.Observer({
      name: 'Duke Univ.',
      lon: -78.93,
      lat: 35.96,
      alt: 110.0
  }),
};


function getCmcStyle() {
  var colorTablePositive = {
    20: '#fee5d9',
    15: '#fcae91',
    10: '#fb6a4a',
     5: '#cb181d',
  };
  var colorTableNegative = {
    20: '#eff3ff',
    15: '#bdd7e7',
    10: '#6baed6',
     5: '#2171b5',
  };

  var context = {
    getSize: function(feature) {
      var magnitudeIndex = feature.attributes.magnitudeIndex;

      if (Math.abs(magnitudeIndex) === 3) {
        return 7.3;
      } else if (Math.abs(magnitudeIndex) === 2) {
        return 4.3;
      } else {
        return 2.5;
      }
    },
    getColor: function(feature) {
      var age = feature.attributes.age;
      var magnitudeIndex = feature.attributes.magnitudeIndex;

      if (magnitudeIndex > 0) {
        if (age > 20) {
          return colorTablePositive[20];
        } else {
          return colorTablePositive[age];
        }
      } else {
        if (age > 20) {
          return colorTableNegative[20];
        } else {
          return colorTableNegative[age];
        }
      }
    },
    getStrokeColor: function(feature) {
      if (feature.attributes.magnitudeIndex > 0) {
        return '#8b0000';
      } else {
        return '#000080';
      }
    },
    getZIndex: function(feature) {
      var age = feature.attributes.age;
      var magnitudeIndex = feature.attributes.magnitudeIndex;

      var magnitudeFactor = 0;
      if (magnitudeIndex > 0) {
        magnitudeFactor = magnitudeIndex + 4;
      } else {
        magnitudeFactor = Math.abs(magnitudeIndex) + 1;
      }

      return (45 - age + 1) * magnitudeFactor;
    }
  };

  style = new OpenLayers.Style({
    pointRadius: '${getSize}',
    fillColor: '${getColor}',
    strokeColor: '${getStrokeColor}',
    graphicZIndex: '${getZIndex}',
    strokeWidth: 1,
    strokeOpacity: 1
  }, {context: context});

  return style;
}


function initBaseLayers() {
  var minZoomLevel = 5;
  var maxZoomLevel = 8;

  var layers = {
    // OpenStreetMap
    openStreetMap: new OpenLayers.Layer.OSM('OpenStreetMap', null, {
      restrictedMinZoom: 5,
      numZoomLevels: 9
    }),
    // Google Maps
    googleMap: new OpenLayers.Layer.Google('Google Maps (Map)', {
      sphericalMercator: true,
      minZoomLevel: minZoomLevel,
      maxZoomLevel: maxZoomLevel
    }),
    // Google Maps (Terrain)
    googleMapTerrain: new OpenLayers.Layer.Google('Google Maps (Terrain)', {
      type: google.maps.MapTypeId.TERRAIN,
      sphericalMercator: true,
      minZoomLevel: minZoomLevel,
      maxZoomLevel: maxZoomLevel
    }),
  };

  for (var i in layers) {
    if (layers.hasOwnProperty(i)) {
      map.addLayer(layers[i]);
    }
  }

  // Set navigation.
  $('select#base-layer').change(function() {
    var key = $('select#base-layer').val();
    map.setBaseLayer(layers[key]);
  });
}


function initWeatherLayers() {
  var layers = {
    // GOES-13 ir4
    goes: new OpenLayers.Layer.WMS('GOES-13 IR4',
      '/wms/goes_ir4?', {
        layers: 'goes_ir4',
        transparent: 'true',
        format: 'image/png'
      }, {
        reproject: false,
        visibility: false,
        isBaseLayer: false,
        opacity: 0.7
    }),
    // NexRad base reflectivity
    nexrad: new OpenLayers.Layer.WMS('NEXRAD Base Reflectivity',
      'http://mesonet.agron.iastate.edu/cgi-bin/wms/nexrad/n0q.cgi?', {
        layers: 'nexrad-n0q-900913-conus',
        transparent: 'true',
        format: 'image/png'
      }, {
        reproject: false,
        visibility: false,
        isBaseLayer: false,
        opacity: 0.7
    }),
    // Air temperature at 2 m
    airtemp: new OpenLayers.Layer.WMS('Air 2m Temperatures',
      'http://mesonet.agron.iastate.edu/cgi-bin/wms/us/obs.cgi?', {
        layers: 'airtemps',
        format: 'image/png',
        transparent: 'true'
      }, {
        visibility: false,
        isBaseLayer: false
    }),
    // Charge moment change
    cmc: new OpenLayers.Layer.Vector('CMC', {
      renderers: ['SVG', 'Canvas'],
      rendererOptions: {zIndexing: true},
      visibility: false,
      styleMap: new OpenLayers.StyleMap(getCmcStyle()),
      strategies: [new OpenLayers.Strategy.Fixed()],
      protocol: new OpenLayers.Protocol.HTTP({
        url: '/cmc/latest-15min.json',
        format: new OpenLayers.Format.GeoJSON({
          internalProjection: new OpenLayers.Projection('EPSG:3857'),
          externalProjection: new OpenLayers.Projection('EPSG:4326')
        })
      })
    }),
  };

  for (var i in layers) {
    if (layers.hasOwnProperty(i)) {
      map.addLayer(layers[i]);
    }
  }

  // Set refresh rates.
  window.setInterval(function() {
    layers.goes.redraw(true);
  }, 310876);
  window.setInterval(function() {
    layers.nexrad.redraw(true);
  }, 300876);
  window.setInterval(function() {
    layers.airtemp.redraw(true);
  }, 301123);
  window.setInterval(function() {
    layers.cmc.refresh();
  }, 311123);

  // Set navigation
  $('input#refresh-layers').click(function() {
    for (var i = 0, n = map.layers.length; i < n; i++) {
      var layer = map.layers[i];
      if (!layer.isBaseLayer && layer.visibility) {
        if (layer.name === 'CMC') {
          layer.refresh();
        } else {
          layer.redraw(true);
        }
      }
    }
  });

  $(':checkbox[name=weather-layer]').click(function() {
    var isChecked = this.checked;
    var key = this.value;
    layers[key].setVisibility(isChecked);
  });
}


function initSiteLayer() {
  var style = new OpenLayers.Style({
    rotation: '${getRotation}'
  }, {
    context: {
      getRotation: function(feature) {
        return feature.attributes.observer.head;
      }
    },
    rules: [
      new OpenLayers.Rule({
        filter: new OpenLayers.Filter.Comparison({
          type: OpenLayers.Filter.Comparison.EQUAL_TO,
          property: 'type',
          value: 'site'
        }),
        symbolizer: {
          externalGraphic: 'static/img/marker.png',
          graphicWidth: 25,
          graphicHeight: 41,
          graphicYOffset: -40
        }
      }),
    ]
  });

  var siteLayer = new OpenLayers.Layer.Vector('Sites', {
    displayInLayerSwitcher: false,
    styleMap: new OpenLayers.StyleMap(style)
  });

  var siteFeatures = [];
  for (var shortName in sites) {
    if (sites.hasOwnProperty(shortName)) {
      var site = sites[shortName];
      siteFeatures.push(new OpenLayers.Feature.Vector(
        site.geometryPoint, {type: 'site', observer: site}));
    }
  }
  siteLayer.addFeatures(siteFeatures);
  map.addLayer(siteLayer);

  var observerControl = new util.ObserverControl();
  map.addControl(observerControl);
  observerControl.setObserver(sites.wiro);

  // Navigation
  $('select#site').change(function() {
    var key = $('select#site').val();
    observerControl.setObserver(sites[key]);
  });
}


function initGridControl() {
  var latLonGridControl = new OpenLayers.Control.Graticule({
    labelFormat: 'd',
    autoActivate: false,
    lineSymbolizer: {
      strokeColor: '#000000',
      strokeWidth: 1.2,
      strokeDashstyle: 'dot'
    },
    labelSymbolizer: {
      fontSize: '0.8em'
    }
  });
  map.addControl(latLonGridControl);

  $('input:checkbox[name=latLonGrid]').change(function() {
    var checked = $('input:checkbox[name=latLonGrid]').is(':checked');
    if (checked) {
      latLonGridControl.activate();
    } else {
      latLonGridControl.deactivate();
    }
  });
}


(function() {
  var EPSG4326 = new OpenLayers.Projection('EPSG:4326');
  var EPSG3857 = new OpenLayers.Projection('EPSG:3857');

  var extent = new OpenLayers.Bounds(-130.0, -30.0, -60.0, 50.0)
    .transform(EPSG4326, EPSG3857);
  var resolutions = [
    4891.9698095703125, 2445.9849047851562, 1222.9924523925781, 611.4962261962891];

  map = new OpenLayers.Map('map', {
    projection: EPSG3857,
    displayProjection: EPSG4326,
    maxExtent: extent,
    restrictedExtent: extent,
    resolutions: resolutions,
    numZoomLevels: 5,
    zoom: 6,
    controls: [
      new OpenLayers.Control.Navigation({zoomWheelEnabled: false}),
      new OpenLayers.Control.Zoom(),
      new OpenLayers.Control.ScaleLine(),
      new OpenLayers.Control.MousePosition({numDigits: 3, emptyString: ""})
    ]
  });

  initBaseLayers();
  initWeatherLayers();
  initSiteLayer();

  initGridControl();
}());
